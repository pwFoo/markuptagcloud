Generate a tagcloud or category -> content lists

Generate a tagcloud with default values - the long way...
~~~~
$mtc = $modules->get('MarkupTagcloud');

// param1: tags parent page, param2: field used for tagging via page references
// parse() adds the two virtual fields "use" (PageArray with pages tagged by this tag) and "useCount" (tagged pages count)
$tags = $mtc->parse('/tags', 'tags');   

// Get all the tags, but ignore unused tags (PW selector!)              
$data = $tags->getTags('useCount>0');

// calculate tag total count
$tagsSum = $data->count();

// 
foreach ($data as $tag) {
    // tagWeight() adds virtual field "weight" to tag (page) object
    $weight = $tags->tagWeight($tag, $tagsSum, 4);
    $values[] = array("tag{$weight}", "tag{$weight}", $tag->url, $tag->title);
}

// render tagcloud, fill placeholders with values 
echo $tags->renderRAW($values, '<li class="%s"><a class="%s" href="%s">%s</a></li>', '<ul>%s</ul>');
~~~~

Shortform to generate the same tagcloud
~~~~
$mtc = $modules->get('MarkupTagcloud');
echo $mtc->parse()->render();
~~~~

Example selector options
~~~~
$mtc->render(null, null, 'sort=-title');
$mtc->render(null, null, 'sort=random, limit=10');
~~~~


Additional sorting, filtering via PW selector and some more options
~~~~
/**
 * Get tags and back references ("tagged" pages) by given defined PW selectors
 * @param string $tagRoot Root page / parent of all the tags
 * @param string $tagField PW page field which references tags
 * @param boolean $backRef Count tag usage only or get the page ids (resource intensive with many pages!)
 * @param array $tagsSelector PW selector to modify the tag selection
 * @param array $backRefSelector PW selector to modify back references / "tagged" pages selection
 * @return object Return object $this to be chainable...
 */
public function parse($tagRoot = null, $tagField = null, $backRef = null, $tagsSelector = null, $backRefSelector = null)

/**
 * Render a cloud / list of tags
 * @param integer $steps Quantity of css classes generated based on tag weight
 * @param string $baseClass Base css class suffix by weight is added to 
 * @param string $selector PW selector to filter / sort tag selection
 * @return string HTML output
 */
public function render($weightSteps = null, $weightClass = null, $selector = null, $innerTpl = null, $outerTpl = null);

/**
 * Render output by given values and templates
 * @param array $data Array of innerTpl placeholder values
 * @param string $innerTpl Template compatible to (v)sprintf
 * @param string $outerTpl Template compatible to (v)sprintf
 * @return string HTML output
 */
public function renderRAW($data, $innerTpl = null, $outerTpl = null);

/**
 * Calculate weight of given tag to build a tag cloud
 * @param object $tag Current tag (page) object
 * @param integer $steps Quantity of steps
 * @param integer $tagsSum Sum of all (selected) tags
 * @return integer Weight of the current tag
 */
public function tagWeight(&$tag, $tagsSum, $steps = null);

/**
 * Get tags optionally filtered by an PW selector
 * @param string $selector Valid PW selector string to use
 * @return object PageArray of tags 
 */
public function getTags($selector = null);
~~~~
